#!/bin/bash

for number in {1..5}
do
	cp data/state_space_$number.txt state_space.txt
	cp data/specification_$number.txt specification.txt
	cp data/L_mapping_$number.txt L_mapping.txt
	cp data/refinement_rounds_$number.txt refinement_rounds.txt
	source env/bin/activate && { env time -v python2 Synthesis.py ; } 2>> logfile.txt && deactivate
	rm state_space.txt specification.txt L_mapping.txt
done

