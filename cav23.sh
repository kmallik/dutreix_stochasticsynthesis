#!/bin/bash
for number in {1..20}
do
	matlab -nodisplay -r "generate_state_space([1,1],1,$number);exit"
 	/usr/bin/time --verbose python2 Synthesis.py >> logfile.txt 2>> logfile.txt
	matlab -nodisplay -r "generate_state_space([1,1],2,$number);exit"
	/usr/bin/time --verbose python2 Synthesis.py >> logfile.txt 2>> logfile.txt

done
exit 0

