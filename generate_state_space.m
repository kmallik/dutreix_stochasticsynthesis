% generating the state space partition for the oscillator example by
% dutreix
%
% author: kaushik

function [] = generate_state_space(varargin)

    if (nargin<2)
        error('Not enough input arguments.\n');
    end
    eta = varargin{1};
    spec = varargin{2};
    if (nargin>=3)
        refinement_rounds = varargin{3};
    end
    
    if (nargin>=4)
        file_suffix = ['_' num2str(varargin{4})];
    else
        file_suffix = '';
    end
    
    fid0=fopen(['./specification' file_suffix '.txt'],'w');
    fprintf(fid0,'%d',spec);
    fclose(fid0);
    lb=[0,0];
    ub=[4,4];
%     firstGridPoint=[round(lb(1)/eta(1))*eta(1), round(lb(2)/eta(2))*eta(2)];
%     lastGridPoint=[round(ub(1)/eta(1))*eta(1), round(ub(2)/eta(2))*eta(2)];
    rad=eta./2;
    firstGridPoint=lb+rad;
    lastGridPoint=ub-rad;

    fid1=fopen(['./state_space' file_suffix '.txt'],'w');
    formatSpec='%1.2f %1.2f %1.2f %1.2f\n';
    fid2=fopen(['./L_mapping' file_suffix '.txt'],'w');

    for lx=firstGridPoint(1)-rad(1):eta(1):lastGridPoint(1)-rad(1)
        for ly=firstGridPoint(2)-rad(2):eta(2):lastGridPoint(2)-rad(2)
            fprintf(fid1,formatSpec,lx,ly,lx+eta(1),ly+eta(2));
            if (spec==1)
                label= findLabel_spec1([lx, ly],[lx+eta(1), ly+eta(2)]);
                fprintf(fid2,'%s\n',label);
            elseif (spec==2)
                label= findLabel_spec2([lx, ly],[lx+eta(1), ly+eta(2)]);
                fprintf(fid2,'%s\n',label);
            end
        end
    end
    fclose(fid1);
    fclose(fid2);
    if (nargin>=3)
        fid3=fopen(['./refinement_rounds' file_suffix '.txt'],'w');
        fprintf(fid3,'%d',refinement_rounds);
        fclose(fid3);
    end
end

function label = findLabel_spec1(lb,ub)
    if ((lb(1)>=1 && ub(1)<=2 && ...
            lb(2)>=1 && ub(2)<=2) || ...
            (lb(1)>=2 && ub(1)<=3 && ...
            lb(2)>=1 && ub(2)<=2) || ...
            (lb(1)>=2 && ub(1)<=3 && ...
            lb(2)>=2 && ub(2)<=3))
        label='A';
    else
        label='';
    end
end


function label = findLabel_spec2(lb,ub)
    if (lb(1)>=0 && ub(1)<=1 && ...
            lb(2)>=0 && ub(2)<=1)
        label='A';
    elseif ((lb(1)>=0 && ub(1)<=1 && ...
            lb(2)>=3 && ub(2)<=4) || ...
            (lb(1)>=1 && ub(1)<=2 && ...
            lb(2)>=1 && ub(2)<=2) || ...
            (lb(1)>=3 && ub(1)<=4 && ...
            lb(2)>=1 && ub(2)<=3))
        label='B';
    elseif (lb(1)>=2 && ub(1)<=3 && ...
            lb(2)>=3 && ub(2)<=4)
        label='C';
    else
        label='';
    end
end
