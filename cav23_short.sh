#!/bin/bash

for number in {3..4}
  do
  for spec in {1..2}
    do
    cp ./data/L_mapping_cav_"${number}"_"${spec}".txt L_mapping.txt
    cp ./data/refinement_rounds_cav_"${number}"_"${spec}".txt  refinement_rounds.txt
    cp ./data/specification_cav_"${number}"_"${spec}".txt specification.txt
    cp ./data/state_space_cav_"${number}"_"${spec}".txt state_space.txt
    /usr/bin/time --verbose python2 Synthesis.py >> logfile.txt 2>> logfile.txt
  done
done
exit 0

